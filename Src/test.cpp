#include"cgc.h"
struct Node {
    int data;
    struct Node* next;
};
static GCState *gc;
Node *InitNode(){
    auto n = (Node*)AllocOther(gc, sizeof(Node));
    n->data = 0;
    n->next = nullptr;
    return n;
}

int main(){
    gc = InitGC();
    auto root = (Node*)AllocRoot(gc, sizeof(Node));
    root->next = nullptr;
    root->data = 1;
    auto cur = root;
    for(auto i = 0; i <10; i++){
        auto next = InitNode();
        next->data = i+1;
        cur->next = next;
        cur = next;
    }
    auto temp = InitNode();
    cur->next = temp;
    temp->data = 100;
    cur = temp;
    for(auto j = 0; j < 5; j++){
        auto n = InitNode();
        cur->next = n;
        n->data = 100+j+1;
    }
    GarbageCollect(gc);
    return 0;
}