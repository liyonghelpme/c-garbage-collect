#pragma once
#include <cstdlib>
struct GCState;
GCState *InitGC();
void GarbageCollect(GCState *gc);
void *AllocRoot(GCState *gc, size_t len);
void *AllocOther(GCState *gc, size_t len);
void GarbageCollect(GCState *gc);
void DumpGarbage(GCState *gc);