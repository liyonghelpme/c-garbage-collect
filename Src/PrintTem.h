#pragma once
#include <iostream>

using namespace std;

void print(){cout<<'\n';}

template<typename T, typename ...TAIL>
void print(const T &t, TAIL... tail)
{
    cout<<t<<' ';
    print(tail...);
}